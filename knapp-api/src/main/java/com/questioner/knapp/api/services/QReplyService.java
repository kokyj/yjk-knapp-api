package com.questioner.knapp.api.services;

import com.questioner.knapp.api.repositories.QReplyRepository;
import com.questioner.knapp.api.repositories.QuestionRepository;
import com.questioner.knapp.api.services.QuestionService;
import com.questioner.knapp.core.models.QReply;
import com.questioner.knapp.core.models.QType;
import com.questioner.knapp.core.models.Question;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Component
@Service
public class QReplyService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
    private QReplyRepository qReplyRepository;
	
	@Autowired
    private QuestionRepository questionRepository;

	public QReply create(QReply data) {
		logger.info("create({})", data);
        QReply qReply = new QReply(data.getText(), data.getDescription(), data.getQuestionID(), data.getEndFlag(), data.getUserID());
        qReply.populateCreateAttributes();
        
		return qReplyRepository.save(qReply);	
	}
	
	public QReply get(Long id) {
		logger.info("get({})", id);
        return qReplyRepository.findOne(id);
	}
	
	//public Long getQuestionID
	
	public QReply update(Long id, QReply data) {
        logger.info("update({},{})", id, data);
        QReply qReply= qReplyRepository.findOne(id);
        if (qReply == null)
            return null;
        else {
        	qReply.setText(data.getText());
        	qReply.setDescription(data.getDescription());
        	//qReply.setEndFlag(data.getEndFlag()); //Set this in another function below
        	qReply.populateUpdateAttributes();
        	qReply = qReplyRepository.save(qReply);
            return qReply;
        }
    }
	
	
	public boolean delete(Long id) {
        logger.info("delete({})", id);
        qReplyRepository.delete(id);
        QReply qReply= qReplyRepository.findOne(id);
        return qReply == null;
    }
	
	public List<QReply> findAll() {
        logger.info("findAll()");
        List<QReply> qReply = new ArrayList<>();
        qReplyRepository.findAll().forEach(qReply::add);
        return qReply;
    }
	
	public QReply setEndFlag(Long id, QReply data) {
		logger.info("update({},{})", id, data);
        QReply qReply= qReplyRepository.findOne(id);
        if (qReply == null)
            return null;
        else {
        	qReply.setEndFlag(data.getEndFlag());
        	qReply.populateUpdateAttributes();
        	qReply = qReplyRepository.save(qReply);
            return qReply;
        }
	}
}
