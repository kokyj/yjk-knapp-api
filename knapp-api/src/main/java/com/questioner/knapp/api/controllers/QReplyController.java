package com.questioner.knapp.api.controllers;

import com.questioner.knapp.api.services.QuestionService;
import com.questioner.knapp.api.services.QReplyService;
import com.questioner.knapp.core.models.QReply;
import com.questioner.knapp.core.models.QType;
import com.questioner.knapp.core.models.Question;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/knapp/qreply")
public class QReplyController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private QReplyService qReplyService;
    
    @Autowired
    private QuestionService questionService;
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<QReply> get(@PathVariable(value = "id") String id) {
        logger.info("get({})", id);
        QReply qReply = qReplyService.get(Long.valueOf(id));
        return (qReply == null) ? new ResponseEntity<>(HttpStatus.NOT_FOUND) : new ResponseEntity<>(qReply, HttpStatus.OK);
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<QReply> create(@RequestBody QReply data) {
        logger.info("create({})", data);
        QReply qReply = qReplyService.create(data);
        return (qReply != null) ? new ResponseEntity<>(qReply, HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<QReply> update(@PathVariable(value = "id") String id, @RequestBody QReply data) {
        logger.info("update({},{})", id, data);
        QReply qReply = qReplyService.update(Long.valueOf(id), data);
        if (qReply == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        else {
            return new ResponseEntity<>(qReply, HttpStatus.OK);
        }
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable(value = "id") String id) {
        logger.info("delete({})", id);
        boolean deleted = qReplyService.delete(Long.valueOf(id));
        return deleted ? new ResponseEntity<>(HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<QReply>> findAll() {
        logger.info("findAll()");
        List<QReply> qReply = qReplyService.findAll();
        if (qReply.size() == 0)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        else
            return new ResponseEntity<>(qReply, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/questions", method = RequestMethod.GET)
    public ResponseEntity<List<Question>> findAllQuestions() {
        logger.info("findAll()");
        List<Question> questions = questionService.findAll();
        if (questions.size() == 0)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        else
            return new ResponseEntity<>(questions, HttpStatus.OK);
    }
}
